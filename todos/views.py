from django.shortcuts import render, get_object_or_404
from todos.models import ToDoList, TodoItem


# Create your views here.
def todo_list_list(request):
    todos = ToDoList.objects.all()
    context = {
        "to_do_list": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo_items(request, id):
    todolist = get_object_or_404(ToDoList, id=id)
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/detail.html", context)
